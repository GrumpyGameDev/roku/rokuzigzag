Sub Main()
    game = CreateGame()
    game.Initialize() 
    game.Execute()
End Sub

Function CreateGame() As Object
    game = {}
    
    game.MessagePort = CreateObject("roMessagePort")
    game.Screen = CreateObject("roScreen")
    game.Screen.SetMessagePort(game.MessagePort)
    game.FontRegistry = CreateObject("roFontRegistry")
    game.FontRegistry.Register("pkg:/locale/default/images/8bitoperator_jve.ttf")
    game.TitleFont = game.FontRegistry.GetFont("8bitoperator JVE",100,false,false)
    
    game.Execute = Execute
    game.Initialize = Initialize
    game.Play = Play
    game.GameOver = GameOver
    game.TitleScreen = TitleScreen
    game.ResetBlocks = ResetBlocks
    game.ResetTail = ResetTail
    
    return game
End Function

Sub Initialize()
    'constant no matter the graphics mode
    m.Columns = 40
    m.Rows=30
    m.TailSize = 5

    'hd values
    m.CellWidth=21
    m.CellHeight=21
    m.BoardX = 220
    m.BoardY=45
    m.BoardWidth = m.CellWidth * m.Columns
    m.BoardHeight = m.CellHeight * m.Rows
    m.BoardCenterX = m.BoardX + m.BoardWidth / 2

    m.TitleFontSize = 200
    m.MenuFontSize = 21
    m.ScoreFontSize = 21

    m.TitleY = m.BoardY + 200
    m.SubTitleY = m.BoardY + 400
    'sd values
    'TODO

    'fonts
    m.TitleFont = m.FontRegistry.GetFont("8bitoperator JVE",m.TitleFontSize,false,false)
    m.MenuFont = m.FontRegistry.GetFont("8bitoperator JVE",m.MenuFontSize,false,false)
    m.ScoreFont = m.FontRegistry.GetFont("8bitoperator JVE",m.ScoreFontSize,false,false)

    'game data    
    m.Blocks = CreateObject("roArray",m.Rows,false)
    m.Tail = CreateObject("roArray",m.TailSize,false)
    m.HighScore = 0

    'TODO: load in high score from file?
End Sub

Sub Execute()
    While True
        m.TitleScreen()
        m.Play()
        m.GameOver()
    End While
End Sub

Sub ResetBlocks()
    For index=0 To m.Rows-1
        m.Blocks[index]=0
    End For
    index = invalid 
End Sub

Sub ResetTail()
    For index=0 To m.TailSize-1
        m.Tail[index]= Int(m.Columns) 
    End For
    index = invalid 
End Sub

Sub TitleScreen()
    m.Screen.Clear(&h808080FF)
    m.Screen.DrawRect(m.BoardX,m.BoardY,m.BoardWidth,m.BoardHeight,&h101010FF)
    DrawTextCC("JetLag",m.BoardCenterX,m.TitleY,&hFFFF,m.TitleFont)
    
    m.Screen.SwapBuffers()
    while true
        msg = wait(0,m.MessagePort)
        if type(msg)="roUniversalControlEvent" then
            Exit while
        end if
    end while
End Sub

Sub Play()
    m.ResetBlocks()
    m.ResetTail()
End Sub

Sub GameOver()
End Sub

Sub DrawTextCC(screen,font,text,color,x,y)
    x = x - font.GetOneLineWidth(text,screen.GetWidth())/2
    y = y - font.GetOneLineHeight()/2
    screen.DrawText(text,x,y,color,font)
End Sub